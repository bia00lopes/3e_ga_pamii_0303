const exp = require('express')
const app = exp()

app.get('/', (req, res) => {
    return res.status(200).json({
        'message': 'Servirdor funcionando!'
    })
})

app.listen(3000, () => {
    console.log('Servidor executando')
})